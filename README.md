# node-yarn Docker images

This repo holds the Dockerfile for base docker images used in all 4ME NodeJS projects.

This Docker image is based off official NodeJS Docker image.

The image contains `yarn` as a package manager.
