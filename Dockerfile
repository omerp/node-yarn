FROM node:7.2.0
MAINTAINER Benjamin BERET <benjamin.beret@aviation-civile.gouv.fr>
RUN npm install -g --progress=false --silent yarn
